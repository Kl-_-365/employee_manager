from django.urls import path, include
from rest_framework.routers import DefaultRouter
from . import views

from .api import EmployeeViewSet, EmployeeViewSetOneLevel

router = DefaultRouter()
router.register(r'all', EmployeeViewSet)

urlpatterns = [
    path('api/', include(router.urls)),
    path('signup', views.SignUp.as_view(), name='signup'),
    path('api/level/<int:level>', EmployeeViewSetOneLevel.as_view(), name='level-api')
]
