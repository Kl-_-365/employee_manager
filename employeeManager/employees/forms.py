from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model

# Get a default model user
User = get_user_model()


# Create a user signup form
class CreationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User
        fields = ('first_name', 'last_name', 'username', 'email', 'is_staff')
