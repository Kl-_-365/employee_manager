from rest_framework import serializers
from djoser.serializers import UserSerializer
from .models import Employee
from django.contrib.auth import get_user_model


# Serialization employees
class EmployeeSerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(read_only=True, default=serializers.CurrentUserDefault(), required=False)

    class Meta:
        model = Employee
        fields = ('full_name', 'position', 'employment_date', 'salary', 'paid_wages', 'user', 'level', 'parent')
        # read_only_fields = ('user')

    def validate_level(self, value):
        if value % 1 != 0 or value < 0 or value > 4:
            raise serializers.ValidationError('level must be: 0, 1, 2, 3, 4')


UserModel = get_user_model()


# Serialization users
class CustomUserSerializer(UserSerializer):
    class Meta:
        model = UserModel
        fields = ('email', 'id', 'first_name', 'last_name', 'username',)
