from rest_framework import viewsets, permissions
from django.contrib.auth import get_user_model
from .serializers import CustomUserSerializer
from djoser.views import UserViewSet

from rest_framework.views import APIView
from rest_framework.response import Response

from .models import Employee
from .serializers import EmployeeSerializer
from .permissions import EmployeeReadOnlyHimself, EmployeeReadOnlyLevel
from django.contrib.auth.models import User


# Display information about employees of one level only
class EmployeeViewSetOneLevel(APIView):
    permission_classes = (EmployeeReadOnlyLevel,)

    def get(self, request, level):
        level = level
        employees = Employee.objects.filter(level=level)
        serializer = EmployeeSerializer(employees, many=True)
        return Response({"employees": serializer.data})


# CRUD for admin users
class EmployeeViewSet(viewsets.ModelViewSet):
    serializer_class = EmployeeSerializer
    queryset = Employee.objects.all()
    permission_classes = (EmployeeReadOnlyHimself,)

    def perform_create(self, serializer):
        self.find_save_user(serializer)

    def perform_update(self, serializer):
        self.find_save_user(serializer)

    # Can link a user by id if any
    def find_save_user(self, serializer):
        users_id = []
        for i in Employee.objects.all():
            if i.user:
                users_id.append(i.user.id)
        if 'user_id' in self.request.data.keys():
            user_id = self.request.data['user_id']
            if user_id not in users_id:
                user = User.objects.filter(id=user_id)[0]
                serializer.save(user=user)
            else:
                serializer.save()


# Api signup
class CreateUserView(UserViewSet):
    model = get_user_model()
    permission_classes = [
        permissions.AllowAny  # Or anon users can't register
    ]
    serializer_class = CustomUserSerializer
