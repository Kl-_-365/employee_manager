from django.contrib import admin
from mptt.admin import MPTTModelAdmin
from .models import Employee
from django.utils.html import format_html
from django.urls import reverse
from .tasks import delete_many_paid_wages


# Create a category Employees in admin
@admin.register(Employee)
class EmployeeAdmin(MPTTModelAdmin):
    # Set visible fields
    list_display = ("full_name", "position", "link_parent", "salary", "paid_wages")
    # Set links
    list_display_links = ["full_name"]
    # Create a search field by name
    search_fields = ("full_name__startswith",)
    # Add filters position, level
    list_filter = ("position", "level")
    # Action that deletes all information about paid salary of all chosen employees
    actions = ["delete_paid_wages"]

    # Make a correct output for a manager and add a link to it
    def link_parent(self, obj):
        if obj.parent:
            name = obj.parent.full_name
            url = (
                reverse("admin:employees_employee_change", args=(obj.parent_id,)))
            return format_html('<a href="{}">{}</a>', url, name)
        else:
            name = "Not Boss"
            return format_html("<b><i>{}</i></b>", name)

    # change label for "parent"
    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        form.base_fields["parent"].label = "Boss:"
        return form

    # Change a name of the column with managers
    link_parent.short_description = "Boss"

    # A function to delete the payroll
    def delete_paid_wages(self, request, queryset):
        quantity_actions = 20
        if len(queryset) > quantity_actions:
            # If a number of actions is exceeded, trigger a dissinchronized execution
            delete_many_paid_wages.delay(queryset.update(paid_wages=0))
        else:
            queryset.update(paid_wages=0)
