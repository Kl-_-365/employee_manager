from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from django.contrib.auth.models import User


# Create a hierarchical model of employees
class Employee(MPTTModel):
    full_name = models.CharField(max_length=256)
    position = models.CharField(max_length=64)
    employment_date = models.DateField()
    salary = models.DecimalField(decimal_places=2, max_digits=9, blank=True)
    paid_wages = models.DecimalField(decimal_places=2, max_digits=9, blank=True)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)

    # Create a restriction/limit on the model level for adding employees who are out of the hierarchy range
    # (do not spawn unlimited levels of employees)
    def save(self, *args, **kwargs):
        max_level = 5
        level = self.parent.level + 1 if self.parent else 0
        print(f'level - {level}')
        if level < max_level:
            super().save(*args, **kwargs)
        else:
            # Comment an error call for testing
            # and set a hierarchy level for those who are out of the zero

            # raise ValueError(f"Maximum hierarchy level reached: {max_level}")
            self.parent = None
            super().save(*args, **kwargs)

    def __str__(self):
        return self.full_name

    class MPTTMeta:
        order_insertion_by = ['full_name']
