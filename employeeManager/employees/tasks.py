from employeeCore.celery import app
from .models import Employee
from django.db.models import F


# Draw the payroll for an employee
@app.task
def payment_of_wages():
    Employee.objects.all().update(paid_wages=F("paid_wages") + F("salary"))


# Start dissinchronized deleting of the payroll
@app.task
def delete_many_paid_wages(func_update):
    pass
