import random

from django_seed import Seed

from employeeManager.employees.models import User, Employee

seeder = Seed.seeder()

seeder.add_entity(User, 20)
seeder.add_entity(Employee, 20, {
    'level': lambda x: random.randint(0, 4),
})
seeder.execute()

inserted_pks = seeder.execute()
