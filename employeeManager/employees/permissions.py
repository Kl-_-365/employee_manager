from rest_framework import permissions
from .models import Employee


# Set a level of permissions (a user can only get info about himself)
class EmployeeReadOnlyHimself(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.is_superuser

    def has_object_permission(self, request, view, obj):
        return obj.user == request.user


# A restriction to see employees of one level (you can see your colleagues and subordinates only)
class EmployeeReadOnlyLevel(permissions.BasePermission):
    def has_permission(self, request, view):
        level = int(str(request)[-3])
        employee = Employee.objects.filter(user=request.user)
        return request.user.is_staff and employee[0].level <= level
