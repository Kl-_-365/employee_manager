from django.views.generic import CreateView
from .forms import CreationForm


# User signup
class SignUp(CreateView):
    form_class = CreationForm
    success_url = ('admin/')
    template_name = 'users/signup.html'
