import requests
import json


# A class for formation and sending of post requests with respect to API
class QueryAPI(object):
    def __init__(self, username='admin', password='password', token=None):
        self.username = username
        self.password = password
        if token:
            self.token = token
        else:
            self.token = self._get_token()

    # Get token
    def _get_token(self, url="http://127.0.0.1:8000/auth-api/jwt/create"):
        url = url
        payload = json.dumps({
            "username": self.username,
            "password": self.password
        })
        headers = {
            'Content-Type': 'application/json'
        }
        response = requests.request("POST", url, headers=headers, data=payload)
        print(response.text)
        return response.json()['access']

    # Send employee
    def send_employee(self, url="http://127.0.0.1:8000/api/all/", test_employee=None):
        headers = {
            'Authorization': 'Bearer ' + self.token,
            'Content-Type': 'application/json'
        }
        self.post_query(url, test_employee, headers)

    # Send user
    def send_user(self, url="http://127.0.0.1:8000/auth-api/users/", test_user=None):
        headers = {
            'Content-Type': 'application/json'
        }
        self.post_query(url, test_user, headers)

    @staticmethod
    def post_query(url, send_data, headers):
        url = url
        payload = json.dumps(send_data)
        headers = headers
        response = requests.request("POST", url, headers=headers, data=payload)

        print(response.text)
