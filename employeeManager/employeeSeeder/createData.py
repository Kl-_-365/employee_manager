import random, string, datetime


# A class for creating random data in various formats. It uses statistics methods
class CreateRandomData(object):
    Alphabet = string.ascii_lowercase

    @staticmethod
    def create_password():
        length = random.randrange(8, 15, 1)
        password = ''.join(
            random.choice('1234567890' + CreateRandomData.Alphabet + '1234567890') for i in range(length))
        return password

    @staticmethod
    def create_username():
        length = random.randrange(8, 15, 1)
        return ''.join(random.choice(CreateRandomData.Alphabet) for i in range(length))

    @staticmethod
    def create_fullName():
        full_name = ''
        for i in range(3):
            full_name += CreateRandomData.create_username().capitalize() + ' '
        return full_name

    @staticmethod
    def create_position():
        positions = {
            0: 'SEO',
            1: 'Vice President',
            2: 'Senior Manager',
            3: 'Manager',
            4: 'Specialist'
        }
        choice = random.randrange(0, 5, 1)
        return positions[choice]

    @staticmethod
    def create_date():
        rand_date = datetime.datetime.now().date() - datetime.timedelta(random.randrange(0, 7500, 1))
        return rand_date.strftime("%Y-%m-%d")

    @staticmethod
    def create_money():
        return random.randrange(1, 900000000, 1) / 100

    @staticmethod
    def create_email():
        return CreateRandomData.create_username() + '@' + CreateRandomData.create_username() + '.by'
