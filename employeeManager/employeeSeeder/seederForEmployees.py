#! / usr / bin / env python3


from sendDataApi import QueryAPI
from testObjects import TestEmployee

# Set a number of objects you want to add
quantity_objects = 20

# Create objects
list_of_employees = [TestEmployee() for i in range(quantity_objects)]

# Create a session for communication with API
api = QueryAPI(username="admin", password="password")

for employee in list_of_employees:
    # Send user
    api.send_user(test_user=employee.user.get_json_data)
    # Send employee
    api.send_employee(test_employee=employee.get_json_data)
