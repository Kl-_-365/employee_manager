import random
from createData import CreateRandomData


# Create an object of a test user
class TestUser(object):
    Id = 1

    def __init__(self):
        self.username = CreateRandomData.create_username()
        self.password = CreateRandomData.create_password()
        self.email = CreateRandomData.create_email()
        self.id = self.__class__.Id + 1
        self.__class__.Id += 1

    # Define an attribute to get data in JSON format
    @property
    def get_json_data(self):
        return {"username": self.username, "password": self.password, "email": self.email}

    # A counter for users
    @classmethod
    def get_id(cls):
        return cls.Id

    def __str__(self):
        return self.username


# Create an object of a test employee
class TestEmployee(object):
    Id = 1

    def __init__(self):
        self.__class__.Id += 1
        self.fullname = CreateRandomData.create_fullName()
        self.position = CreateRandomData.create_position()
        self.employment_date = CreateRandomData.create_date()
        self.salary = CreateRandomData.create_money()
        self.paid_wages = CreateRandomData.create_money()
        self.parent = self.parent_choice()
        self.user = TestUser()

    def __str__(self):
        return self.fullname

    # Define an attribute to get data in JSON format
    @property
    def get_json_data(self):
        return {
            "full_name": self.fullname,
            "position": self.position,
            "employment_date": self.employment_date,
            "salary": self.salary,
            "paid_wages": self.paid_wages,
            "parent": self.parent,
            "user_id": self.user.id
        }

    # Select a random manager
    def parent_choice(self):
        if self.__class__.Id <= 3:
            return None
        else:
            return random.randrange(1, TestEmployee.Id - 1, 1)

    # A counter for employees
    @classmethod
    def get_id(cls):
        return cls.Id
