"""
Файл настроек Celery
https://docs.celeryproject.org/en/stable/django/first-steps-with-django.html
"""
from __future__ import absolute_import
import os
from celery import Celery
from celery.schedules import crontab

# Set settings Django for 'celery'
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'employeeCore.settings')
app = Celery("employeeCore")
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

# Set a timeout for task execution equalled to 2 hours
app.conf.beat_schedule = {
    'payment_of_wages_every_2_hours': {
        'task': 'employees.tasks.payment_of_wages',
        'schedule': crontab(minute=0, hour='*/2')
    }
}
